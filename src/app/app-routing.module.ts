import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { AdminEditQuestionComponent } from './admin-edit-question/admin-edit-question.component';
import { AdminAddAdminComponent } from './admin-add-admin/admin-add-admin.component';
import { AdminEditAdminComponent } from './admin-edit-admin/admin-edit-admin.component';
import { AdminAddQuestionComponent } from './admin-add-question/admin-add-question.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './Service/auth-guard.service';
import { QuestionDetailsComponent } from './question-details/question-details.component';
import { HomeCategory1McComponent } from './home-category1-mc/home-category1-mc.component';
import { HomeCategory1Component } from './home-category1/home-category1.component';
import { WebboardComponent } from './webboard/webboard.component';
import { HomeCategory2Component } from './home-category2/home-category2.component';
import { HomeCategory3Component } from './home-category3/home-category3.component';
import { HomeCategory4Component } from './home-category4/home-category4.component';
import { HomeCategory5Component } from './home-category5/home-category5.component';
import { HomeCategory6Component } from './home-category6/home-category6.component';
import { HomeCategory2McComponent } from './home-category2-mc/home-category2-mc.component';
import { HomeCategory3McComponent } from './home-category3-mc/home-category3-mc.component';
import { HomeCategory4McComponent } from './home-category4-mc/home-category4-mc.component';
import { HomeCategory5McComponent } from './home-category5-mc/home-category5-mc.component';
import { HomeCategory6McComponent } from './home-category6-mc/home-category6-mc.component';
import { WbAskQuestionComponent } from './wb-ask-question/wb-ask-question.component';
import { UserloginComponent } from './login/userlogin/userlogin.component';
import { webboard } from './interfaces/webboard';
import { WebboardAdminComponent } from './webboard-admin/webboard-admin.component';
import { WebboardAdminAddAnswerComponent } from './webboard-admin-add-answer/webboard-admin-add-answer.component';
import { SocialAuthService } from 'angularx-social-login';
import { AuthGuardGoogleSerivceService } from './Service/auth-guard-google-serivce.service';
import { componentFactoryName } from '@angular/compiler';
import { WebboardMcComponent } from './webboard-mc/webboard-mc.component';


const routes: Routes = [  
  {path: 'admin/login', redirectTo: 'admin/login', pathMatch: 'full'},
  {path:'user/login', redirectTo: 'user/login', pathMatch: 'full'},
  //Admin(main)
  {path:'admin/home', component: AdminhomeComponent, canActivate:[AuthGuardService] },
  {path:'admin/question/edit/:id', component: AdminEditQuestionComponent, canActivate:[AuthGuardService]},
  {path:'admin/question/add', component: AdminAddQuestionComponent, canActivate:[AuthGuardService]},
  {path:'admin/question/details/:id', component: QuestionDetailsComponent,canActivate:[AuthGuardService]},
  //Admin(user)
  {path:'admin/user/add', component: AdminAddAdminComponent, canActivate:[AuthGuardService]},
  {path:'admin/user', component: AdminProfileComponent, canActivate:[AuthGuardService]},
  {path:'admin/user/edit/:id', component: AdminEditAdminComponent, canActivate:[AuthGuardService]},
  //Admin(Webboard)
  {path: 'admin/webboard', component: WebboardAdminComponent, canActivate:[AuthGuardService]},
  {path: 'admin/webboard/addAnswer/:id', component: WebboardAdminAddAnswerComponent, canActivate:[AuthGuardService]},

  //Login
  {path:'admin/login', component: LoginComponent},
  {path:'user/login', component: UserloginComponent},
  //User(home)
  {path:'home', component: HomeComponent},
  {path:'home/category1', component: HomeCategory1Component},
  {path:'home/category2', component: HomeCategory2Component},
  {path:'home/category3', component: HomeCategory3Component},
  {path:'home/category4', component: HomeCategory4Component},
  {path:'home/category5', component: HomeCategory5Component},
  {path:'home/category6', component: HomeCategory6Component},
  {path:'home/category1/mc/:id', component: HomeCategory1McComponent},
  {path:'home/category2/mc/:id', component: HomeCategory2McComponent},
  {path:'home/category3/mc/:id', component: HomeCategory3McComponent},
  {path:'home/category4/mc/:id', component: HomeCategory4McComponent},
  {path:'home/category5/mc/:id', component: HomeCategory5McComponent},
  {path:'home/category6/mc/:id', component: HomeCategory6McComponent},
  //Webboard
  {path:'webboard', component: WebboardComponent},
  {path:'webboard/ask', component: WbAskQuestionComponent, canActivate:[AuthGuardGoogleSerivceService]},
  {path:'webboard/detail/:id', component: WebboardMcComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
