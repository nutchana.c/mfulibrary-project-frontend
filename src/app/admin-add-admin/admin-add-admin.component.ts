import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Admin } from '../interfaces/Admin';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../Service/api.service';
import { AuthenticationService } from '../Service/authentication.service';


@Component({
  selector: 'app-admin-add-admin',
  templateUrl: './admin-add-admin.component.html',
  styleUrls: ['./admin-add-admin.component.scss']
})
export class AdminAddAdminComponent implements OnInit {

  adminForm: FormGroup;
  admins: Admin[] = [];

  constructor( public fb: FormBuilder,
    private router: Router,
    public apiService: ApiService,
    private loginservice: AuthenticationService) { }

  ngOnInit() {
    this.adminForm = this.fb.group({
      username: [null, Validators.required],
      fullname: [null, Validators.required],
      password: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      google_id: [null, [Validators.required, Validators.email]],
      thumbnail: [null, Validators.required],
  
    })
    
    console.log(this.adminForm);

    this.apiService.getAllAdmin().subscribe((data: Admin[])=>{
    this.admins = data; 
     console.log(data);
 }) 
  }

  submitForm() {
    this.apiService.addAdmin(this.adminForm.value).subscribe(res => {
      alert('SUCCESS!!')
      
      this.router.navigateByUrl('admin/user')
      
  }
)
}
get f() { return this.adminForm.controls; }

logout(){
  this.loginservice.logOut()
  this.router.navigate(['admin/login']);
}
}
