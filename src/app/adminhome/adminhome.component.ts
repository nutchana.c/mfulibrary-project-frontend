import { Component, OnInit } from '@angular/core';
import { ApiService } from '../Service/api.service';
import { Question } from '../interfaces/question';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../Service/authentication.service';


@Component({
  selector: 'app-adminhome',
  templateUrl: './adminhome.component.html',
  styleUrls: ['./adminhome.component.scss']
})
export class AdminhomeComponent implements OnInit {
  questions: Question[] = [];
  isLoadingResults = true;

  constructor(public apiService: ApiService, 
    private router: Router, 
    private loginservice: AuthenticationService) { }

  ngOnInit() {
    this.apiService.getAllQuestion()
    .subscribe((data: Question[])=>{
      this.questions = data; 
    }) ;
  
  }

  deleteQuestion(id: any) {
    this.isLoadingResults = true;
    this.apiService.deleteQuestion(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['admin/home']);
          window.location.reload()
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }
  logout(){
    this.loginservice.logOut();
    this.router.navigate(['admin/login']);
  }
}
