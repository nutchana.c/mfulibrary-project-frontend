import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory5Component } from './home-category5.component';

describe('HomeCategory5Component', () => {
  let component: HomeCategory5Component;
  let fixture: ComponentFixture<HomeCategory5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
