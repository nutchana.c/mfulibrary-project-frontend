import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory3McComponent } from './home-category3-mc.component';

describe('HomeCategory3McComponent', () => {
  let component: HomeCategory3McComponent;
  let fixture: ComponentFixture<HomeCategory3McComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory3McComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory3McComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
