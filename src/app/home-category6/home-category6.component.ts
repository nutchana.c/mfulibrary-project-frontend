import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from '../interfaces/question';
import { ApiService } from '../Service/api.service';

@Component({
  selector: 'app-home-category6',
  templateUrl: './home-category6.component.html',
  styleUrls: ['./home-category6.component.scss']
})
export class HomeCategory6Component implements OnInit {
  questions: Question[] = [];
  questionForm: Question;
  _id: any;

  constructor(private apiService:ApiService,
    private route:ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.apiService.getQuestionByisPublic6()
    .subscribe((data: Question[])=>{
      this.questions = data;
      console.log(this.questions);
    });
  }

}
