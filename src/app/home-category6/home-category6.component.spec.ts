import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory6Component } from './home-category6.component';

describe('HomeCategory6Component', () => {
  let component: HomeCategory6Component;
  let fixture: ComponentFixture<HomeCategory6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
