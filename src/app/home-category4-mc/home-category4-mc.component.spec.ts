import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory4McComponent } from './home-category4-mc.component';

describe('HomeCategory4McComponent', () => {
  let component: HomeCategory4McComponent;
  let fixture: ComponentFixture<HomeCategory4McComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory4McComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory4McComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
