import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of, Subject } from 'rxjs';
import { Question } from '../interfaces/question';
import { catchError, tap } from 'rxjs/operators';
import { Admin } from '../interfaces/Admin';
import { webboard } from '../interfaces/webboard';
import {emailDto} from '../interfaces/emailDto';
import { stringify } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  private apiServer = "http://localhost:8080";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  
  constructor(private http: HttpClient) { }

  getAllQuestion(): Observable<Question[]>{
    return this.http.get<Question[]>(this.apiServer+'/question/questions')
    .pipe(
      catchError(this.errorHandler)
    )
  }
  
  create(question: string): Observable<Question>{
    return this.http.post<Question>(this.apiServer + '/question/addQuestion', JSON.stringify(question), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getQuestionById(id: any): Observable<Question>{
    return this.http.get<Question>(this.apiServer+'/question/questions/' + id )
    .pipe(
      catchError(this.errorHandler)
    )
  }

  edit(id: any, question: string): Observable<Question>{
    return this.http.post<Question>(this.apiServer+'/question/editQuestion/' + id, JSON.stringify(question), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  deleteQuestion(id: any){
    return this.http.delete<Question>(this.apiServer + '/question/deleteQuestion/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  //Admin
  getAllAdmin(): Observable<Admin[]>{
    return this.http.get<Admin[]>(this.apiServer+'/admin/admins')
    .pipe(
      catchError(this.errorHandler)
    )
  
  }

  addAdmin(admin: string): Observable<Admin>{
    return this.http.post<Admin>(this.apiServer + '/admin/add', JSON.stringify(admin), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getAdminById(id: any): Observable<Admin>{
    return this.http.get<Admin>(this.apiServer+'/admin/admins/' + id )
    .pipe(
      catchError(this.errorHandler)
    )
  }

  editAdmin(id: any, admin: string): Observable<Admin>{
    return this.http.post<Admin>(this.apiServer+'/admin/edit/' + id, JSON.stringify(admin), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  deleteAdmin(id: any){
    return this.http.delete<Admin>(this.apiServer + '/admin/delete/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  
  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }

 //WebboardAdmin
  getAllQuestionWb(): Observable<webboard[]>{
      return this.http.get<webboard[]>(this.apiServer+'/webBoard/question')
      .pipe(
        catchError(this.errorHandler)
      )
  }

  getQuestionWbById(id: any): Observable<webboard>{
    return this.http.get<webboard>(this.apiServer+'/webBoard/question/' + id )
    .pipe(
      catchError(this.errorHandler)
    )
  }

  addAnswerWb(id: any, answer: string): Observable<webboard>{
    return this.http.post<webboard>(this.apiServer+'/webBoard/admin/addAnswer/' + id, JSON.stringify(answer), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getAllQuestionisPublic_Ans(): Observable<webboard[]>{
    return this.http.get<webboard[]>(this.apiServer+'/webBoard/question/public')
    .pipe(
      catchError(this.errorHandler)
    )
}

getAllAnswerisNull(): Observable<webboard[]>{
  return this.http.get<webboard[]>(this.apiServer+'/webBoard/question/answerNull')
  .pipe(
    catchError(this.errorHandler)
  )
}
getAllQuestionIsNotPublic_Ans(): Observable<webboard[]>{
  return this.http.get<webboard[]>(this.apiServer+'/webBoard/question/answerNotNull')
  .pipe(
    catchError(this.errorHandler)
  )
}

  deleteQuestionWb(id: any){
    return this.http.delete<webboard>(this.apiServer + '/webBoard/admin/delete/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

 //Webboard
  getQuestionWbByisPublic(): Observable<webboard[]>{
    return this.http.get<webboard[]>(this.apiServer+'/webBoard/question/public/score')
    .pipe(
      catchError(this.errorHandler)
    )
  }

  likeQuestionWbById(id: any): Observable<webboard>{
    return this.http.post<webboard>(this.apiServer + '/webBoard/user/score/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  addQuestionWebboard(question: string): Observable<webboard>{
    return this.http.post<webboard>(this.apiServer + '/webBoard/user/addQuestion', JSON.stringify(question), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

 //User
  public getQuestionByisPublic1(){
    return this.http.get("http://localhost:8080/question/questions/public/topic1");
  }

  public getQuestionByisPublic2(){
    return this.http.get("http://localhost:8080/question/questions/public/topic2");
  }

  public getQuestionByisPublic3(){
    return this.http.get("http://localhost:8080/question/questions/public/topic3");
  }

  public getQuestionByisPublic4(){
    return this.http.get("http://localhost:8080/question/questions/public/topic4");
  }

  public getQuestionByisPublic5(){
    return this.http.get("http://localhost:8080/question/questions/public/topic5");
  }

  public getQuestionByisPublic6(){
    return this.http.get("http://localhost:8080/question/questions/public/topic6");
  }

  sendEmail(emailDto: string){
    return this.http.post("http://localhost:8080/email/send/add", JSON.stringify(emailDto), this.httpOptions);
  }

}
