import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";

export class User {
  constructor(public status: string) {}
}

@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  constructor(private httpClient: HttpClient) {}
// Provide username and password for authentication, and once authentication is successful, 
//store JWT token in session
  authenticate(username, password) {
    return this.httpClient
      .post<any>("http://localhost:8080/admin/login", { username, password },{responseType: 'text' as 'json' })
      .pipe(
        map(userData => {
          sessionStorage.setItem("username", username);
          let tokenStr = userData;
          sessionStorage.setItem("token", tokenStr);
          return userData;
        })
      );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem("token");
    // console.log(!(user === null));
    return !(user === null);
  }

  isGoogleLoggedIn(){

    let user = sessionStorage.getItem("socialusers");
    return !(user === null);
  }

  logOut() {
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("username");
    sessionStorage.removeItem("socialusers")
  }
}