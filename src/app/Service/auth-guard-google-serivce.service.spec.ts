import { TestBed } from '@angular/core/testing';

import { AuthGuardGoogleSerivceService } from './auth-guard-google-serivce.service';

describe('AuthGuardGoogleSerivceService', () => {
  let service: AuthGuardGoogleSerivceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthGuardGoogleSerivceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
