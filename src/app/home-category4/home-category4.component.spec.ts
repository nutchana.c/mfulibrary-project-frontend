import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory4Component } from './home-category4.component';

describe('HomeCategory4Component', () => {
  let component: HomeCategory4Component;
  let fixture: ComponentFixture<HomeCategory4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
