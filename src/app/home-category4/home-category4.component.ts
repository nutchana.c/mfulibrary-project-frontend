import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from '../interfaces/question';
import { ApiService } from '../Service/api.service';

@Component({
  selector: 'app-home-category4',
  templateUrl: './home-category4.component.html',
  styleUrls: ['./home-category4.component.scss']
})
export class HomeCategory4Component implements OnInit {
  questions: Question[] = [];
  questionForm: Question;
  _id: any;
  
  constructor(private apiService:ApiService,
    private route:ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.apiService.getQuestionByisPublic4()
    .subscribe((data: Question[])=>{
      this.questions = data;
      console.log(this.questions);
    });
  }

}
