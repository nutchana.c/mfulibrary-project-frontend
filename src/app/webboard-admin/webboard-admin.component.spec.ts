import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebboardAdminComponent } from './webboard-admin.component';

describe('WebboardAdminComponent', () => {
  let component: WebboardAdminComponent;
  let fixture: ComponentFixture<WebboardAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebboardAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebboardAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
