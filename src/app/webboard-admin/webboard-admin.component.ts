import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from '../interfaces/question';
import { webboard } from '../interfaces/webboard';
import { ApiService } from '../Service/api.service';
import { AuthenticationService } from '../Service/authentication.service';

@Component({
  selector: 'app-webboard-admin',
  templateUrl: './webboard-admin.component.html',
  styleUrls: ['./webboard-admin.component.scss']
})
export class WebboardAdminComponent implements OnInit {
  publics: webboard[] = [];
  notAnswers: webboard[] = [];
  answer_notPublics:webboard[] = [];
  
  isLoadingResults = true;

  constructor(public apiService: ApiService, 
    private router: Router, 
    private loginservice: AuthenticationService) { }

  ngOnInit() {


    this.apiService.getAllQuestionisPublic_Ans()
    .subscribe((data: webboard[])=>{
      this.publics = data; 
    }) ;

    this.apiService.getAllAnswerisNull()
    .subscribe((data: webboard[])=>{
      this.notAnswers = data; 
    }) ;

    this.apiService.getAllQuestionIsNotPublic_Ans()
    .subscribe((data: webboard[])=>{
      this.answer_notPublics = data; 
    }) ;
  
  }

  deleteQuestion(id: any) {
    this.isLoadingResults = true;
    this.apiService.deleteQuestion(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['admin/home']);
          window.location.reload()
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }
  logout(){
    this.loginservice.logOut();
    this.router.navigate(['admin/login']);
  }

}
