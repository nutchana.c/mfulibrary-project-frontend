import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { webboard } from '../interfaces/webboard';
import { ApiService } from '../Service/api.service';

@Component({
  selector: 'app-webboard',
  templateUrl: './webboard.component.html',
  styleUrls: ['./webboard.component.scss']
})
export class WebboardComponent implements OnInit {
  questionWb: webboard[] = [];
  private user: SocialUser;
  private loggedIn: boolean;
  

  constructor(private apiService:ApiService,
    private router: Router,
    private authService: SocialAuthService ) { }

  ngOnInit() {
    this.apiService.getQuestionWbByisPublic()
    .subscribe((data: webboard[])=>{
      this.questionWb = data;
      console.log(this.questionWb);
    })

  }

  likeQuestionWbById(id: any) {
    this.apiService.likeQuestionWbById(id)
    .subscribe(res => {
      window.location.reload();
    })
  }

}
