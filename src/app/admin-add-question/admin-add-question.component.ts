import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../Service/api.service';
import { Question } from '../interfaces/question';
import { AuthenticationService } from '../Service/authentication.service';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';

import ImageResize from 'quill-image-resize-module';
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
Quill.register('modules/imageResize', ImageResize)

import { ImageDrop } from 'quill-image-drop-module';
import { emailDto } from '../interfaces/emailDto';
Quill.register('modules/imageDrop', ImageDrop)

@Component({
  selector: 'app-admin-add-question',
  templateUrl: './admin-add-question.component.html',
  styleUrls: ['./admin-add-question.component.scss']
})

export class AdminAddQuestionComponent implements OnInit {
  questionForm: FormGroup;
  questions: Question[] = [];
  adminDisplayName = '';
  editorText = '';


  constructor(
    public fb: FormBuilder,
    private router: Router,
    public apiService: ApiService,
    private loginservice: AuthenticationService) { }



  changedEditor(event: EditorChangeContent | EditorChangeSelection){
    // console.log(' editor got changed', event);
    this.editorText = event['editor']['root']['innerHTML'];
  }

  editorModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, {'header': 2 }],
      [{ 'list': 'ordered'}, {'list': 'bullet' }],
      [{ 'script': 'sub'}, {'script': 'super' }],
      [{ 'indent': '-1'}, {'indent': '+1'}],
      [{ 'direction': 'rtl'}],

      [{ 'size': ['small', false, 'large', 'huge'] }],
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],

      ['link', 'image', 'video'],
      
    ],
    imageResize: true,
    imageDrop: true
};

  ngOnInit() {
    this.adminDisplayName = sessionStorage.getItem('username');

    this.questionForm = this.fb.group({
    topic: [null, Validators.required],
    question: [null, Validators.required],
    answer: [null, Validators.required],
    isPublic: [null, Validators.required],
    addDate: [null, Validators.required],
    updateDate: [null, Validators.required],
    lastUpdater: [null, Validators.required]   
    }
    )
    // console.log(this.questionForm);

    this.apiService.getAllQuestion().subscribe((data: Question[])=>{
      this.questions = data; 
    }) 
  }

    submitForm() {
    this.apiService.create(this.questionForm.value).subscribe(res => {
      alert('SUCCESS!!')
      this.router.navigateByUrl('admin/home')

      
  })
}

get f() { return this.questionForm.controls; }

    logout(){
    this.loginservice.logOut()
    this.router.navigate(['admin/login']);
  }

  
}
