import { Component, OnInit } from '@angular/core';
import { Question } from '../interfaces/question';
import { ApiService } from '../Service/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Admin } from '../interfaces/Admin';
import { AuthenticationService } from '../Service/authentication.service';


@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {

  admins: Admin[] = [];
  isLoadingResults = true;

  constructor(public apiService: ApiService, private router: Router, private route: ActivatedRoute,  private loginservice: AuthenticationService) { }

  ngOnInit() {
    this.apiService.getAllAdmin().subscribe((data: Admin[])=>{
      this.admins = data; 
      console.log(data);  
    }) ;
  }

  deleteAdmin(id: any) {
    this.isLoadingResults = true;
    this.apiService.deleteAdmin(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          alert('SUCCESS!!')
          this.router.navigate(['admin/user']);
          window.location.reload()
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
      
  }
  logout(){
    this.loginservice.logOut()
    this.router.navigate(['admin/login']);
  }
}
