import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Admin } from '../interfaces/Admin';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../Service/api.service';
import { AuthenticationService } from '../Service/authentication.service';


@Component({
  selector: 'app-admin-edit-admin',
  templateUrl: './admin-edit-admin.component.html',
  styleUrls: ['./admin-edit-admin.component.scss']
})
export class AdminEditAdminComponent implements OnInit {
  adminForm: FormGroup;
  admins: Admin[] = [];
  _id: '';

  constructor(public fb: FormBuilder,
    private router: Router,
    public apiService: ApiService,
    private route: ActivatedRoute,
    private loginservice: AuthenticationService) { }

  ngOnInit() {
    this.getAdminById(this.route.snapshot.params['id']);
    this.adminForm = this.fb.group({
      username: ['', Validators.required],
      fullname: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.required],
      google_id: ['', Validators.required],
      thumbnail: ['', Validators.required],  
      })
  }

  getAdminById(id: any) {
    this.apiService.getAdminById(id).subscribe((data: any) => {
      this._id = data.id;
      this.adminForm.setValue({
        username: data.username,
        fullname: data.fullname,
        password: data.password,
        email: data.email,
        google_id: data.google_id,
        thumbnail: data.thumbnail,
      });
    });
  }

  onFormSubmit() {
    this.apiService.editAdmin(this._id, this.adminForm.value)
      .subscribe((res: any) => {
          const id = res._id;
          alert('SUCCESS!!')
          this.router.navigate(['/admin/user']);
        }, (err: any) => {
          console.log(err); 
        }
      );
      }
      isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
      }
      logout(){
        this.loginservice.logOut()
        this.router.navigate(['admin/login']);
      }

}
