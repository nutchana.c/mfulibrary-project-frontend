import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditAdminComponent } from './admin-edit-admin.component';

describe('AdminEditAdminComponent', () => {
  let component: AdminEditAdminComponent;
  let fixture: ComponentFixture<AdminEditAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
