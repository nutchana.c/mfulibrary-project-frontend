import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory1McComponent } from './home-category1-mc.component';

describe('HomeCategory1McComponent', () => {
  let component: HomeCategory1McComponent;
  let fixture: ComponentFixture<HomeCategory1McComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory1McComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory1McComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
