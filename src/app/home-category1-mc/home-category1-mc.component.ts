import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Question } from '../interfaces/question';
import { ApiService } from '../Service/api.service';

@Component({
  selector: 'app-home-category1-mc',
  templateUrl: './home-category1-mc.component.html',
  styleUrls: ['./home-category1-mc.component.scss']
})
export class HomeCategory1McComponent implements OnInit {
  questions1:any;
  questionForm: Question;
  _id: '';
  adminDisplayName = '';
  mySubscription: any;

  constructor(private apiService:ApiService,
    private route:ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.adminDisplayName = sessionStorage.getItem('username');

    let resp1=this.apiService.getQuestionByisPublic1();
    resp1.subscribe((data)=>this.questions1=data);

    this.getQuestionById(this.route.snapshot.params['id']);
    this.questionForm = new Question;
              
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });

  }

  getQuestionById(id: any){
    this.apiService.getQuestionById(id)
    .subscribe((data: any) => {
      this._id = data.id;
      this.questionForm = data;
      // console.log(data);
    }, (err) => {
      console.log(err);        
      }
    );
  }

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }


}
