import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from './Service/api.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { AdminEditQuestionComponent } from './admin-edit-question/admin-edit-question.component';

import { AdminAddAdminComponent } from './admin-add-admin/admin-add-admin.component';
import { AdminEditAdminComponent } from './admin-edit-admin/admin-edit-admin.component';
import { AdminAddQuestionComponent } from './admin-add-question/admin-add-question.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { BasicAuthHtppInterceptorServiceInterceptor } from './basic-auth-htpp-interceptor-service.interceptor';
import { QuillModule } from 'ngx-quill';
import { QuestionDetailsComponent } from './question-details/question-details.component';
import { HomeCategory1Component } from './home-category1/home-category1.component';
import { HomeCategory1McComponent } from './home-category1-mc/home-category1-mc.component';
import { WebboardComponent } from './webboard/webboard.component';
import { HomeCategory2Component } from './home-category2/home-category2.component';
import { HomeCategory3Component } from './home-category3/home-category3.component';
import { HomeCategory4Component } from './home-category4/home-category4.component';
import { HomeCategory5Component } from './home-category5/home-category5.component';
import { HomeCategory6Component } from './home-category6/home-category6.component';
import { HomeCategory2McComponent } from './home-category2-mc/home-category2-mc.component';
import { HomeCategory3McComponent } from './home-category3-mc/home-category3-mc.component';
import { HomeCategory4McComponent } from './home-category4-mc/home-category4-mc.component';
import { HomeCategory5McComponent } from './home-category5-mc/home-category5-mc.component';
import { HomeCategory6McComponent } from './home-category6-mc/home-category6-mc.component';
import { WbAskQuestionComponent } from './wb-ask-question/wb-ask-question.component';
import { UserloginComponent } from './login/userlogin/userlogin.component';
import { WebboardAdminComponent } from './webboard-admin/webboard-admin.component';
import { WebboardAdminAddAnswerComponent } from './webboard-admin-add-answer/webboard-admin-add-answer.component';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { CoolSocialLoginButtonsModule } from '@angular-cool/social-login-buttons';
import { DatePipe } from '@angular/common';
import { WebboardMcComponent } from './webboard-mc/webboard-mc.component';


@NgModule({
  declarations: [
    AppComponent, 
    HomeComponent, 
    AdminhomeComponent, 
    AdminEditQuestionComponent, 
    AdminAddQuestionComponent, 
    AdminAddAdminComponent, 
    AdminEditAdminComponent, 
    AdminProfileComponent, 
    LoginComponent, 
    UserloginComponent,
    QuestionDetailsComponent, 
    HomeCategory1Component,
    HomeCategory2Component, 
    HomeCategory3Component, 
    HomeCategory4Component, 
    HomeCategory5Component, 
    HomeCategory6Component, 
    HomeCategory1McComponent, 
    HomeCategory2McComponent, 
    HomeCategory3McComponent, 
    HomeCategory4McComponent, 
    HomeCategory5McComponent, 
    HomeCategory6McComponent, 
    WebboardComponent, 
    WbAskQuestionComponent, 
    WebboardAdminComponent,  
    WebboardAdminAddAnswerComponent, WebboardMcComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SocialLoginModule,
    CoolSocialLoginButtonsModule,
    QuillModule.forRoot()
  ],
    
  providers: [ApiService,BasicAuthHtppInterceptorServiceInterceptor,DatePipe,{
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: true,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(
            '900397720446-dbe1o5sqq0f7ugd5i78h4caqm85h0dms.apps.googleusercontent.com'
          ),
        }
        
      ]
    } as SocialAuthServiceConfig,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }


