import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory6McComponent } from './home-category6-mc.component';

describe('HomeCategory6McComponent', () => {
  let component: HomeCategory6McComponent;
  let fixture: ComponentFixture<HomeCategory6McComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory6McComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory6McComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
