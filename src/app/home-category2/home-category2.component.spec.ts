import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory2Component } from './home-category2.component';

describe('HomeCategory2Component', () => {
  let component: HomeCategory2Component;
  let fixture: ComponentFixture<HomeCategory2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
