import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../Service/api.service';
import { Question } from '../interfaces/question';
import { AuthenticationService } from '../Service/authentication.service';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';

import ImageResize from 'quill-image-resize-module';
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
Quill.register('modules/imageResize', ImageResize)

import { ImageDrop } from 'quill-image-drop-module';
import { webboard } from '../interfaces/webboard';
import { emailDto } from '../interfaces/emailDto';
Quill.register('modules/imageDrop', ImageDrop)

@Component({
  selector: 'app-webboard-admin-add-answer',
  templateUrl: './webboard-admin-add-answer.component.html',
  styleUrls: ['./webboard-admin-add-answer.component.scss']
})
export class WebboardAdminAddAnswerComponent implements OnInit {

  questionForm: FormGroup;
  questions: webboard[] = [];
  _id: '';
  adminDisplayName = '';
  editorText = '';
  emailDtoForm: FormGroup;
  emailDto: emailDto[] = [];

  changedEditor(event: EditorChangeContent | EditorChangeSelection){
    // console.log(' editor got changed', event);
    this.editorText = event['editor']['root']['innerHTML'];
  }

  editorModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, {'header': 2 }],
      [{ 'list': 'ordered'}, {'list': 'bullet' }],
      [{ 'script': 'sub'}, {'script': 'super' }],
      [{ 'indent': '-1'}, {'indent': '+1'}],
      [{ 'direction': 'rtl'}],

      [{ 'size': ['small', false, 'large', 'huge'] }],
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],

      ['link', 'image', 'video']
    ],
    imageResize: true,
    imageDrop: true
  };

  constructor(public fb: FormBuilder,
    private router: Router,
    public apiService: ApiService,
    private route: ActivatedRoute,
    private loginservice: AuthenticationService) { }

  ngOnInit() { 

    this.getQuestionWbById(this.route.snapshot.params['id']);
    this.questionForm = this.fb.group({
      _id:['',Validators.required],
      question: ['', Validators.required],
      questionDate: ['', Validators.required],
      answer: ['', Validators.required],
      answerDate: ['', Validators.required],
      updater_user: ['', Validators.required],
      updater_admin: ['', Validators.required],
      updater_admin1: ['', Validators.required],
      isPublic: ['', Validators.required],   
      score_like: ['', Validators.required] 
      });
      
      this.emailDtoForm = this.fb.group({
          to : ['', Validators.required],
          from : ['testermfulibrary@gmail.com', Validators.required],
          question : ['', Validators.required],
          answer : ['', Validators.required],
          subject : ['', Validators.required], 

      });


      this.adminDisplayName = sessionStorage.getItem('username');
  }

  getQuestionWbById(id: any) {
    this.apiService.getQuestionWbById(id).subscribe((data: any) => {
      this._id = data.id;
      this.questionForm.setValue({
        _id: data.id,
        question: data.question,
        questionDate: data.questionDate,
        answer: data.answer,
        answerDate: data.answerDate,
        updater_user: data.updater_user,
        updater_admin: this.adminDisplayName,
        updater_admin1: data.updater_admin,
        isPublic: data.isPublic,
        score_like: data.score_like
      });
    });
    
  }

//   sendEmail(id:any) {
//   this.apiService.getQuestionWbById(id).subscribe((data1: any) => {
//     this._id = data1.id;
//     this.emailDtoForm.setValue({
//       to: data1.updater_user,
//       question: data1.question,
      
//     })
//   });
// }

  onFormSubmit() {
    this.apiService.addAnswerWb(this._id, this.questionForm.value)
      .subscribe((res: any) => {
          const id = res._id;
          alert('SUCCESS!!')
          this.router.navigate(['/admin/webboard']);
        }, (err: any) => {
          console.log(err); 
        }
      );
  }
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
  logout(){
    this.loginservice.logOut()
    this.router.navigate(['admin/login']);
  }

  sendEmail(){
    this.apiService.sendEmail(this.emailDtoForm.value)
    .subscribe((res:any) =>{
      alert('SEND EMAIL SUCCESS!!')
    })
  }

  deleteQuestionWb(id: any) {
    this.apiService.deleteQuestionWb(id)
      .subscribe(res => {
        alert('DELETE SUCCESS!!')
          this.router.navigate(['admin/webboard']);
        }, (err) => {
          // console.log(err);
        }
      );
  }
}


