import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebboardAdminAddAnswerComponent } from './webboard-admin-add-answer.component';

describe('WebboardAdminAddAnswerComponent', () => {
  let component: WebboardAdminAddAnswerComponent;
  let fixture: ComponentFixture<WebboardAdminAddAnswerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebboardAdminAddAnswerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebboardAdminAddAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
