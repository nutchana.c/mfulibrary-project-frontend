import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory3Component } from './home-category3.component';

describe('HomeCategory3Component', () => {
  let component: HomeCategory3Component;
  let fixture: ComponentFixture<HomeCategory3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
