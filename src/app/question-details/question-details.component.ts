import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from '../interfaces/question';
import { ApiService } from '../Service/api.service';
import { AuthenticationService } from '../Service/authentication.service';

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.scss']
})
export class QuestionDetailsComponent implements OnInit {
  _id: '';
  questionForm: Question;
  
  constructor(
    public apiService: ApiService, 
    private router: Router, 
    private route: ActivatedRoute, 
    private loginservice: AuthenticationService) { }

  ngOnInit() {
    this.questionForm = new Question;
    this._id = this.route.snapshot.params['id'];

    this.apiService.getQuestionById(this._id)
      .subscribe(data => {
        // console.log(data)
        this.questionForm = data;
      }, error => 
      console.log(error));   
  }
  
  deleteQuestion(id: any) {
    this.apiService.deleteQuestion(id)
      .subscribe(res => {
        alert('DELETE SUCCESS!!')
          this.router.navigate(['admin/home']);
        }, (err) => {
          // console.log(err);
        }
      );
  }

  logout(){
    this.loginservice.logOut();
    this.router.navigate(['admin/login']);
  }
}
