import { Component, OnInit } from '@angular/core';
import { Question } from '../interfaces/question';
import { ApiService } from '../Service/api.service';

@Component({
  selector: 'app-home-category1',
  templateUrl: './home-category1.component.html',
  styleUrls: ['./home-category1.component.scss']
})
export class HomeCategory1Component implements OnInit {
  questions: Question[] = [];
  _id: any;
  
  constructor(private apiService:ApiService) { }

  ngOnInit() {
    this.apiService.getQuestionByisPublic1()
    .subscribe((data: Question[])=>{
      this.questions = data;
      console.log(this.questions);
    });

  }
  
}
