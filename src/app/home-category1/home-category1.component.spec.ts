import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory1Component } from './home-category1.component';

describe('HomeCategory1Component', () => {
  let component: HomeCategory1Component;
  let fixture: ComponentFixture<HomeCategory1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
