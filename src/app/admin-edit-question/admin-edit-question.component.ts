import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../Service/api.service';
import { Question } from '../interfaces/question';
import { AuthenticationService } from '../Service/authentication.service';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';

import ImageResize from 'quill-image-resize-module';
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
Quill.register('modules/imageResize', ImageResize)

import { ImageDrop } from 'quill-image-drop-module';
Quill.register('modules/imageDrop', ImageDrop)

@Component({
  selector: 'app-admin-edit-question',
  templateUrl: './admin-edit-question.component.html',
  styleUrls: ['./admin-edit-question.component.scss']
})
export class AdminEditQuestionComponent implements OnInit {
  questionForm: FormGroup;
  questions: Question[] = [];
  _id: '';
  editorText = '';
  adminDisplayName = '';

  changedEditor(event: EditorChangeContent | EditorChangeSelection){
    // console.log(' editor got changed', event);
    this.editorText = event['editor']['root']['innerHTML'];
  }

  editorModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, {'header': 2 }],
      [{ 'list': 'ordered'}, {'list': 'bullet' }],
      [{ 'script': 'sub'}, {'script': 'super' }],
      [{ 'indent': '-1'}, {'indent': '+1'}],
      [{ 'direction': 'rtl'}],

      [{ 'size': ['small', false, 'large', 'huge'] }],
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean'],

      ['link', 'image', 'video']
    ],
    imageResize: true,
    imageDrop: true
  };

  constructor(public fb: FormBuilder,
    private router: Router,
    public apiService: ApiService,
    private route: ActivatedRoute,
    private loginservice: AuthenticationService) { }

  ngOnInit() {
    this.adminDisplayName = sessionStorage.getItem('username');

    this.getQuestionById(this.route.snapshot.params['id']);
    this.questionForm = this.fb.group({
      topic: [null, Validators.required],
      question: [null, Validators.required],
      answer: [null, Validators.required],
      isPublic: [null, Validators.required],
      addDate: [null, Validators.required],
      updateDate: [null, Validators.required],
      lastUpdater: [null, Validators.required]   
      })
  
  }

  getQuestionById(id: any) {
    this.apiService.getQuestionById(id).subscribe((data: any) => {
      this._id = data.id;
      this.questionForm.setValue({
        topic: data.topic,
        question: data.question,
        answer: data.answer,
        isPublic: data.isPublic,
        addDate: data.addDate,
        updateDate: data.updateDate,
        lastUpdater: this.adminDisplayName
      });
    });
  }

  onFormSubmit() {
    this.apiService.edit(this._id, this.questionForm.value)
      .subscribe((res: any) => {
          const id = res._id;
          alert('SUCCESS!!')
          this.router.navigate(['/admin/home']);
        }, (err: any) => {
          console.log(err); 
        }
      );
  }

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
  logout(){
    this.loginservice.logOut()
    this.router.navigate(['admin/login']);
  }
}

