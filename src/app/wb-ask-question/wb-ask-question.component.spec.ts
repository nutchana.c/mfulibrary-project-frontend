import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WbAskQuestionComponent } from './wb-ask-question.component';

describe('WbAskQuestionComponent', () => {
  let component: WbAskQuestionComponent;
  let fixture: ComponentFixture<WbAskQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WbAskQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WbAskQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
