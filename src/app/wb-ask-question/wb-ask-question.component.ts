import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';
import { webboard } from '../interfaces/webboard';
import { ApiService } from '../Service/api.service';
import { AuthenticationService } from '../Service/authentication.service';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-wb-ask-question',
  templateUrl: './wb-ask-question.component.html',
  styleUrls: ['./wb-ask-question.component.scss'],

})
export class WbAskQuestionComponent implements OnInit {

  questionForm: FormGroup;
  questions: webboard[] = [];
  user: SocialUser;
  loggedIn: boolean;
  editorText = '';

  constructor(private router: Router,
    private authService: SocialAuthService,
    private loginservice: AuthenticationService,
    public fb: FormBuilder,
    public apiService: ApiService,
) {}

    changedEditor(event: EditorChangeContent | EditorChangeSelection){
      // console.log(' editor got changed', event);
      this.editorText = event['editor']['root']['innerHTML'];
    }
  
    editorModules = {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],
        ['blockquote', 'code-block'],
  
        [{ 'header': 1 }, {'header': 2 }],
        [{ 'list': 'ordered'}, {'list': 'bullet' }],
        [{ 'script': 'sub'}, {'script': 'super' }],
        [{ 'indent': '-1'}, {'indent': '+1'}],
        [{ 'direction': 'rtl'}],
  
        [{ 'size': ['small', false, 'large', 'huge'] }],
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  
        [{ 'color': [] }, { 'background': [] }],
        [{ 'font': [] }],
        [{ 'align': [] }],
  
        ['clean'],
  
        ['link', 'image', 'video'],
        
      ],
      imageResize: true,
      imageDrop: true
  };

  ngOnInit(): void {    
    this.questionForm = this.fb.group({
      question: [null, Validators.required],
      questionDate: [null, Validators.required],
      updater_user: [null, Validators.required],
      
  })

    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
  })

}
submitForm() {
  this.apiService.addQuestionWebboard(this.questionForm.value).subscribe(res => {
    alert('SUCCESS!!')
    this.router.navigateByUrl('webboard')

    
})

}

get f() { return this.questionForm.controls; }

    logout(){
    this.loginservice.logOut()
    this.router.navigate(['webboard']);
  }

}