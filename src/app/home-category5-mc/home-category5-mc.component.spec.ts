import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory5McComponent } from './home-category5-mc.component';

describe('HomeCategory5McComponent', () => {
  let component: HomeCategory5McComponent;
  let fixture: ComponentFixture<HomeCategory5McComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory5McComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory5McComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
