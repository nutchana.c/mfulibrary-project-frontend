import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { webboard } from '../interfaces/webboard';
import { ApiService } from '../Service/api.service';

@Component({
  selector: 'app-webboard-mc',
  templateUrl: './webboard-mc.component.html',
  styleUrls: ['./webboard-mc.component.scss']
})
export class WebboardMcComponent implements OnInit {
  _id: '';
  questionForm: webboard;
  questionWb: webboard[] = [];
  adminDisplayName = '';

  constructor(private apiService:ApiService,
    private route:ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.adminDisplayName = sessionStorage.getItem('username');

    
    this.getQuestionWbById(this.route.snapshot.params['id']);
    this.questionForm = new webboard;
  }

  getQuestionWbById(id: any){
    this.apiService.getQuestionWbById(id)
    .subscribe((data:any) => {
      this._id = data.id;
      this.questionForm = data;
     
    })
  }

  // likeQuestionWbById(id: any) {
  //   this.apiService.likeQuestionWbById(id)
  //   .subscribe((data:any) => {
  //     this._id = data.id;
  //     this.questionForm = data;
  //   }) 
  // }
  likeQuestionWbById(id: any) {
    this.apiService.likeQuestionWbById(id)
    .subscribe((res:any) => {
      this._id = res._id;
      this.questionForm = res;
      window.location.reload();
    })
  }

}
