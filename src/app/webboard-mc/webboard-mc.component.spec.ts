import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebboardMcComponent } from './webboard-mc.component';

describe('WebboardMcComponent', () => {
  let component: WebboardMcComponent;
  let fixture: ComponentFixture<WebboardMcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebboardMcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebboardMcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
