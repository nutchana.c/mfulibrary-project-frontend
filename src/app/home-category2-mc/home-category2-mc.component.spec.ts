import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCategory2McComponent } from './home-category2-mc.component';

describe('HomeCategory2McComponent', () => {
  let component: HomeCategory2McComponent;
  let fixture: ComponentFixture<HomeCategory2McComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCategory2McComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCategory2McComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
