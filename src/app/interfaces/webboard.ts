export class webboard {
    _id: number;
    question: string;
    questionDate: string;
    answer: string;
    answerDate: string;
    updater_user: string;
    updater_admin: string;
    isPublic: boolean;
    score_like: number;
}