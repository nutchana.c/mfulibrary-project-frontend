export class Admin {

    _id: Number;
    username: string;
    fullname: string;
    password: string;
    email: string;
    google_id: string;
    thumbnail: Number;

   
}
