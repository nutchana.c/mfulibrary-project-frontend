export class Question {
    _id: number;
    topic: string;
    question: string;
    answer: string;
    isPublic: boolean;
    addDate: string;
    updateDate: string;
    lastUpdater: string;
   
}
