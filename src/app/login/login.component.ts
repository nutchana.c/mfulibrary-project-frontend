import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../Service/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username = ''
  password = ''
  invalidLogin = false
  
  @Input() error: string | null;

  constructor(
    private router: Router,
    private loginservice: AuthenticationService
    ) {}

  ngOnInit() {
    
  }

  checkLogin() {
    (this.loginservice.authenticate(this.username, this.password)
    .subscribe(
      data => {
        this.router.navigate(['admin/home'])
        this.invalidLogin = false
      },
      error => { 
        this.invalidLogin = true
        this.error= "username or password incorrect";

      }
      
    )
    
    ); 
    
  }
  
}

