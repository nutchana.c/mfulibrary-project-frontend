import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/Service/authentication.service';
import { SocialAuthService, SocialUser } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";


@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.scss']
})
export class UserloginComponent implements OnInit {
  username = ''
  password = ''
  invalidLogin = false
  user: SocialUser;
  loggedIn: boolean;
  
  @Input() error: string | null;
  
  constructor(
    private router: Router,
    private loginservice: AuthenticationService,
    private authService: SocialAuthService,
    
    ) {}

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
    this.user = user;
    this.loggedIn = (user != null);
  });
  }

  checkLogin() {
    (this.loginservice.authenticate(this.username, this.password)
    .subscribe(
      data => {
        this.router.navigate(['admin/home'])
        this.invalidLogin = false
      },
      error => { 
        this.invalidLogin = true
        this.error= "username or password incorrect";
        }   
      )  
    );  
  }

  signInWithGoogle() {
    (this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
      data => {
        sessionStorage.setItem('socialusers', JSON.stringify( this.user));
        this.router.navigate(['/webboard/ask'])
        this.invalidLogin = false
      },
      error => { 
        this.invalidLogin = true
        this.error= "username or password incorrect";

      }
      
    ));
  }
  
}

